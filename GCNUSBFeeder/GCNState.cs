﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GCNUSBFeeder
{
    public class ControllerOffsets
    {
        public int analogXOffset;
        public double analogXScalar;
        public int analogYOffset;
        public double analogYScalar;
        public int cstickXOffset;
        public double cstickXScalar;
        public int cstickYOffset;
        public double cstickYScalar;
        public int analogLOffset;
        public double analogLScalar;
        public int analogROffset;
        public double analogRScalar;

        public const double SCALAR_BASE = 1;

        public ControllerOffsets()
        {
            analogXOffset = 0;
            analogXScalar = 1;
            analogYOffset = 0;
            analogYScalar = 1;
            cstickXOffset = 0;
            cstickXScalar = 1;
            cstickYOffset = 0;
            cstickYScalar = 1;
            analogLOffset = 0;
            analogLScalar = 1;
            analogROffset = 0;
            analogRScalar = 1;
        }
        public void CalibrateOffsets(int atX, int atY, int atcX, int atcY, int atL, int atR)
        {
            //analogXOffset = (127 - atX);
            //analogYOffset = (127 - atY);
            analogXOffset = (-atX);
            analogYOffset = (-atY);
            cstickXOffset = (-atcX);
            cstickYOffset = (-atcY);
            analogLOffset = (-atL);
            analogROffset = (-atR);

            //analogXScalar = (SCALAR_BASE + Math.Abs(analogXOffset) / 127);
            //analogYScalar = (SCALAR_BASE + Math.Abs(analogYOffset) / 127);
            //cstickXScalar = (SCALAR_BASE + Math.Abs(cstickXOffset) / 127);
            //cstickYScalar = (SCALAR_BASE + Math.Abs(cstickYOffset) / 127);
            //analogLScalar = (SCALAR_BASE + Math.Abs(analogLOffset) / 255);
            //analogRScalar = (SCALAR_BASE + Math.Abs(analogROffset) / 255);
        }
    }
    public class GCNState
    {
        public int unpluggedSince;
        public ControllerOffsets offsets;       
        public int analogX;
        public int analogY;

        public int cstickX;
        public int cstickY;

        public int analogL;
        public int analogR;

        public bool A;
        public bool B;
        public bool X;
        public bool Y;
        public bool Z;
        public bool R;
        public bool L;
        public bool start;

        public bool up;
        public bool left;
        public bool down;
        public bool right;

        public int POVstate;

        public const int STATE_CONNECTED = 1;
        public const int STATE_WAIT = 2;
        public const int STATE_DISCONNECTED = 0;
        public const int STATE_JUST_CONNECTED = 3;
        public const int STATE_UNKNOWN = -1;
        public const int FRAMES_TILL_TIMEOUT = 10;

        public GCNState()
        {
            offsets = new ControllerOffsets();
            analogX = 127;
            analogY = 127;
            cstickX = 127;
            cstickY = 127;
            analogL = 0;
            analogR = 0;
            A       = false;
            B       = false;
            X       = false;
            Y       = false;
            Z       = false;
            R       = false;
            L       = false;
            start   = false;
            up      = false;
            left    = false;
            down    = false;
            right   = false;
            //Keeps track of how long the controller has been unplugged.
            //We don't care about units of time, just the number
            //of consecutive inputs.
            unpluggedSince = FRAMES_TILL_TIMEOUT + 1;
        }

        public int GetState(byte controllerState)
        {
            if (controllerState == 20)
            {
                if (unpluggedSince < FRAMES_TILL_TIMEOUT)
                {
                    //Has been plugged in
                    unpluggedSince = 0;
                    return STATE_CONNECTED;
                }
                else
                {
                    unpluggedSince = 0;
                    return STATE_JUST_CONNECTED;
                }
            }
            else if (controllerState == 4)
            {
                if (unpluggedSince >= FRAMES_TILL_TIMEOUT)
                {
                    //Is not plugged in
                    return STATE_DISCONNECTED;
                }
                unpluggedSince += 1;
                //Might still be plugged in
                return STATE_WAIT;
            }
            //Unknown State
            return STATE_UNKNOWN;
        }
        public void SetNeutral()
        {
            analogX = 127;
            analogY = 127;
            cstickX = 127;
            cstickY = 127;
            analogL = 0;
            analogR = 0;
            A = false;
            B = false;
            X = false;
            Y = false;
            Z = false;
            R = false;
            L = false;
            start = false;
            up = false;
            left = false;
            down = false;
            right = false;
        }

        public void UpdateController(byte[] input)
        {
            var state = GetState(input[0]);
            switch (state)
            {
                case STATE_DISCONNECTED:
                    SetNeutral();
                    break;
                case STATE_CONNECTED:
                    RefreshState(input, false);
                    break;
                case STATE_JUST_CONNECTED:
                    RefreshState(input, true);
                    break;
                case STATE_WAIT:
                    break;
                case STATE_UNKNOWN:
                    SetNeutral();
                    break;
                default:
                    break;
            }
                    
        }

        public void RefreshState(byte[] input, bool forceCalibrate)
        {
            //[0] joystick enabled
            //[1] upper end D-Pad, lower end a,b,x,y
            //[2] R button, L Button, z, start
            //[3] analog X
            //[4] analog Y
            //[5] c-stick X
            //[6] c-stick Y
            //[7] L axis
            //[8] R Axis

            //[1] [0]: A
            //    [1]: B
            //    [2]: X
            //    [3]: Y
            //    [4]: Left
            //    [5]: Right
            //    [6]: Down
            //    [7]: Up

            //[2] [0]: start
            //    [1]: z
            //    [2]: R button
            //    [3]: L Button
            //    [4]: not used
            //    [5]: not used
            //    [6]: not used
            //    [7]: not used

            if (input.Length == 9)
            {
                if ((int)input[0] > 0)
                {
                    byte b1 = input[1];
                    A = (b1 & (1 << 0)) != 0;
                    B = (b1 & (1 << 1)) != 0;
                    X = (b1 & (1 << 2)) != 0;
                    Y = (b1 & (1 << 3)) != 0;

                    left = (b1 & (1 << 4)) != 0;
                    right = (b1 & (1 << 5)) != 0;
                    down = (b1 & (1 << 6)) != 0;
                    up = (b1 & (1 << 7)) != 0;

                    //Generate POV state for vJoy.
                    if (right) { POVstate = 1; }
                    else if (down) { POVstate = 2; }
                    else if (left) { POVstate = 3; }
                    else if (up) { POVstate = 0; }
                    else { POVstate = -1; }

                    byte b2 = input[2];
                    start = (b2 & (1 << 0)) != 0;
                    Z = (b2 & (1 << 1)) != 0;
                    R = (b2 & (1 << 2)) != 0;
                    L = (b2 & (1 << 3)) != 0;

                    //Logic for centering the controller.
                    //We'll do the math on calculating the offset constants here so that we don't have to do it every cycle.

                    if (forceCalibrate)
                    {
                        offsets.CalibrateOffsets((int)input[3], (int)input[4], (int)input[5], (int)input[6], (int)input[7], (int)input[8]);
                    }

                    analogX = (int)(((int)input[3] + offsets.analogXOffset) * offsets.analogXScalar) + 127;
                    analogY = (int)(((int)input[4] + offsets.analogYOffset) * offsets.analogYScalar) + 127;
                    cstickX = (int)(((int)input[5] + offsets.cstickXOffset) * offsets.cstickXScalar) + 127;
                    cstickY = (int)(((int)input[6] + offsets.cstickYOffset) * offsets.cstickYScalar) + 127;
                    analogL = (int)(((int)input[7] + offsets.analogLOffset) * offsets.analogLScalar);
                    analogR = (int)(((int)input[8] + offsets.analogROffset) * offsets.analogRScalar);
                }
            }
            else
            {
                throw new Exception("Invalid byte array for input");
            }
        }
    }
}
